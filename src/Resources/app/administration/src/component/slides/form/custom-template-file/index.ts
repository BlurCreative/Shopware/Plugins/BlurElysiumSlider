import template from './template.html.twig'

const { Component, Mixin } = Shopware 
const { mapMutations, mapState } = Component.getComponentHelper()

export default Component.wrapComponentConfig({
    template,

    mixins: [
        Mixin.getByName('placeholder'),
        Mixin.getByName('blur-device-utilities')
    ],

    computed: {

        ...mapState('blurElysiumSlide', [
            'slide',
            'currentDevice'
        ]),
    },

    methods: {

        ...mapMutations('blurElysiumSlide', [
            'setSlide'
        ]),
    },
})
